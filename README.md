# IMPORTANT

Ce projet a été réalisé dans le cadre de l'UE Moteur 3D en utilisant le moteur Goblim du laboratoire XLim. Ainsi le code présent dans ce dépôt correspond uniquement au travail que j'ai réalisé et ne peut donc pas fonctionner sans le reste du moteur.

# ORGANISATION

Les fichiers sont organisés dans les dossiers suivants :

* Effects : contient les effets post-process
* Materials : contient les classes de matériaux
* Objets : contient les objets utilisés dans le projet
* Sources : contient le code de base
* Textures

# AUTRES

[Rapport du projet avec références](http://pierrethuillier.fr/files/rapports/WaterGoblim.pdf)