#ifndef _PLUIE_EFFECT_
#define _PLUIE_EFFECT_

#include "Engine\OpenGL\EffectGL.h"
#include "Engine\Base\Scene.h"

class Pluie : public EffectGL {

public :
	Pluie(std::string name, float width, float height);
	~Pluie();

	virtual void apply(GPUFBO* in, GPUFBO *out=NULL);

private:
	GLProgram *vp;
	GLProgram *fp;

	float width;
	float height;

	GPUTexture2D *texPluie;
	GPUsampler *texSampler;

	GPUsampler *gBuffer;
	GPUFBO *fbo;

	GPUvec2* directionGPU;
	glm::vec2 direction;

};
#endif