#version 430
#extension GL_ARB_shading_language_include : enable
#extension GL_ARB_bindless_texture : enable
#include "/Materials/Common/Common"
#line 5

layout(std140) uniform CPU
{
	sampler2D pluieTex;
};

uniform sampler2D texFBO;

in vec3 texCoords;
in vec3 texRain;
in vec3 myPos;

out vec4 Color;

void main()
{

	Color = texture(texFBO, texCoords.xy);
	
	vec4 vCol = texture(pluieTex, texRain.xy);

	//Si le pixel courant de la texture de pluie a une valeur en rouge c'est que le pixel est noir
	//On ajoute donc rien � la couleur
	if(vCol.r == 1.0)
		vCol = vec4(0.0);

	Color = Color + vCol;	

}