#version 430

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };

layout(std140) uniform CPU
{
	vec2 direction;
};

layout (location = 0) in vec3 Position;
layout (location = 3) in vec3 textCoord;

out vec3 texCoord;
out vec3 texRain;
out vec3 myPos;

void main()
{
	vec3 myPos = Position;

	gl_Position = vec4(myPos,1.0);

	texCoord = textCoord;
	texRain = texCoord;

	//On calcule la nouvelle position de la texture d'image
	texRain.x = texCoord.x + direction.x;
	texRain.y = texCoord.y + direction.y;

}