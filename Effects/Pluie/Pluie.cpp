#include "Pluie.h"

Pluie::Pluie(std::string name, float width, float height) : EffectGL(name, "Pluie") {

	vp = new GLProgram(this->m_ClassName + "-Base", GL_VERTEX_SHADER);
	fp = new GLProgram(this->m_ClassName + "-Display", GL_FRAGMENT_SHADER);

	m_ProgramPipeline->useProgramStage(GL_VERTEX_SHADER_BIT, vp);
	m_ProgramPipeline->useProgramStage(GL_FRAGMENT_SHADER_BIT, fp);
	m_ProgramPipeline->link();

	Scene *scene = Scene::getInstance();

	this->width = width;
	this->height = height;

	fbo = new GPUFBO("Pluie");
	fbo->create(width, height);

	texPluie = new GPUTexture2D("C:/Home/Output/SampleProject/Debug/Textures/pluie.png");
	texSampler = fp->uniforms()->getGPUsampler("pluieTex");
	texSampler->Set(texPluie->getHandle());

	direction = glm::vec2(0.00, 0.00);
	directionGPU = vp->uniforms()->getGPUvec2("direction");
	directionGPU->Set(direction);

}

Pluie::~Pluie(){
	delete vp;
	delete fp;
}

void Pluie::apply(GPUFBO* in, GPUFBO *out)
{

	in->getColorTexture(0)->bind();

	direction.y += 0.01;
	direction.x -= 0.001;

	directionGPU = vp->uniforms()->getGPUvec2("direction");
	directionGPU->Set(direction);

	m_ProgramPipeline->bind();
	quad->drawGeometry(GL_TRIANGLES);
	m_ProgramPipeline->release();
	
	in->getColorTexture(0)->release();
	
}