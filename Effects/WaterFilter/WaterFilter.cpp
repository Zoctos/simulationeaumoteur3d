#include "WaterFilter.h"

WaterFilter::WaterFilter(std::string name) : EffectGL(name, "WaterFilter") {

	vp = new GLProgram(this->m_ClassName + "-Base", GL_VERTEX_SHADER);
	fp = new GLProgram(this->m_ClassName + "-Display", GL_FRAGMENT_SHADER);

	m_ProgramPipeline->useProgramStage(GL_VERTEX_SHADER_BIT, vp);
	m_ProgramPipeline->useProgramStage(GL_FRAGMENT_SHADER_BIT, fp);
	m_ProgramPipeline->link();

	Scene *scene = Scene::getInstance();

	texSampler = fp->uniforms()->getGPUsampler("texFBO");
	texSampler->Set(0);

	filterColor = glm::vec3(0.0, 0.125, 0.25);
	gpuFilter = fp->uniforms()->getGPUvec3("filterColor");
	gpuFilter->Set(filterColor);

}

WaterFilter::~WaterFilter() {
	delete vp;
	delete fp;
}

void WaterFilter::setFilterColor(glm::vec3 filter) {
	gpuFilter->Set(filter);
}

void WaterFilter::apply(GPUFBO* in, GPUFBO *out)
{
	in->getColorTexture(0)->bind();

	texSampler->Set(0);

	m_ProgramPipeline->bind();
	quad->drawGeometry(GL_TRIANGLES);
	m_ProgramPipeline->release();

	in->getColorTexture(0)->release();

}