#ifndef _WATERFILTER_EFFECT_
#define _WATERFILTER_EFFECT_

#include "Engine\OpenGL\EffectGL.h"
#include "Engine\Base\Scene.h"

class WaterFilter : public EffectGL {

public:
	WaterFilter(std::string name);
	~WaterFilter();

	virtual void apply(GPUFBO* in, GPUFBO *out = NULL);

	void setFilterColor(glm::vec3 filter);

private:
	GLProgram *vp;
	GLProgram *fp;

	GPUsampler *texSampler;

	GPUvec3 *gpuFilter;
	glm::vec3 filterColor;

};
#endif