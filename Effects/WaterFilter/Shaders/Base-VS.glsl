#version 430

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };

layout (location = 0) in vec3 Position;
layout (location = 3) in vec3 textCoord;

out vec3 texCoord;

void main()
{
	gl_Position = vec4(Position,1.0);

	texCoord = textCoord;

}