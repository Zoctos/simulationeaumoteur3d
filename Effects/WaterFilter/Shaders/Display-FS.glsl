#version 430
#extension GL_ARB_shading_language_include : enable
#extension GL_ARB_bindless_texture : enable
#include "/Materials/Common/Common"
#line 5

layout(std140) uniform CPU
{
	vec3 filterColor;
};

uniform sampler2D texFBO;

in vec3 texCoords;

out vec4 Color;

void main()
{

	vec4 vCol = texture(texFBO, texCoords.xy);

	//On ajoute la couleur du filtre � la couleur de la sc�ne
	vCol.rgb += filterColor;

	//Si on d�passe 1.0 sur l'un des canaux on remet la valeur � 1.0
	if(vCol.r > 1.0)
		vCol.r = 1.0;
	if(vCol.b > 1.0)
		vCol.b = 1.0;
	if(vCol.g > 1.0)
		vCol.g = 1.0;

	Color = vCol;

}