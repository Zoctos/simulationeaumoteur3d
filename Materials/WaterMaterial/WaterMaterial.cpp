#include "WaterMaterial.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


WaterMaterial::WaterMaterial(std::string name, glm::vec4 & c, bool isWaveMove) :
	MaterialGL(name, "WaterMaterial")
{

	posLight = glm::vec3(0.0, 0.0, 0.0);
	coefMat = glm::vec3(0.2, 0.6, 0.2);

	pointPosition = glm::vec3(0.0, 0.0, 0.0);
	wave = glm::vec3(0, 0, 0);
	time = 0.0f;
	this->isWaveMove = isWaveMove;

	pointPos = vp->uniforms()->getGPUvec3("pointPosition");
	positionWave = vp->uniforms()->getGPUvec3("positionWave");
	timeCPU = vp->uniforms()->getGPUfloat("time");
	waveMove = vp->uniforms()->getGPUbool("isWaveMove");

	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	color = fp->uniforms()->getGPUvec4("CPU_color");
	color->Set(glm::vec4(0.3, 0.5, 0.8, 1.0));
	
	coef = fp->uniforms()->getGPUvec3("coefMat");
	coef->Set(coefMat);
	cameraPos = fp->uniforms()->getGPUvec3("cameraPos");
	cameraPos->Set(glm::vec3(1.0, 1.0, 1.0));

	light = fp->uniforms()->getGPUvec3("posLight");
	light->Set(posLight);

	waterTexture = new GPUTexture2D("C:/Home/Output/SampleProject/Debug/Textures/water.png");
	samplerTexture = fp->uniforms()->getGPUsampler("samplerTexture");
	samplerTexture->Set(waterTexture->getHandle());

	pointPos->Set(pointPosition);
	positionWave->Set(wave);
	timeCPU->Set(0);

	gpuMethodWave = vp->uniforms()->getGPUbool("methodWave");
	gpuMethodWave->Set(methodWave);

	gpuWaveSpeed = vp->uniforms()->getGPUfloat("speed");
	gpuWaveSpeed->Set(0.0f);

	timeProgress = true;

}
WaterMaterial::~WaterMaterial()
{

}

void WaterMaterial::setColor(glm::vec4 & c)
{
	color->Set(c);
}

void WaterMaterial::setCameraPos(glm::vec3 &camPos)
{
	cameraPos->Set(camPos);
}

void WaterMaterial::setPosLight(glm::vec3 &posLight)
{
	light->Set(posLight);
}

void WaterMaterial::setPosWave(glm::vec3 &posWave) {
	wave = posWave;
}

void WaterMaterial::setPointPosition(glm::vec3 &pointPos) {
	pointPosition = pointPos;
}

void WaterMaterial::setIsWaveMove(bool &isWaveMove) {
	this->isWaveMove = isWaveMove;
}

void WaterMaterial::setMethodWave(bool method) {
	this->gpuMethodWave->Set(method);
}

void WaterMaterial::setWaveSpeed(float speed) {
	this->gpuWaveSpeed->Set(speed);
}

void WaterMaterial::render(Node *o)
{
	if (m_ProgramPipeline)
	{
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void WaterMaterial::update(Node* o, const int elapsedTime)
{
	modelViewProj->Set(o->frame()->getTransformMatrix());

	if (timeProgress) {
		time+= 0.005;
		wave.x += 0.1;
	}
	else {
		time-= 0.005;
		wave.x -= 0.1;
	}
	
	if (time > 1000 || time < -2000)
		timeProgress = !timeProgress;

	timeCPU->Set(time);
	positionWave->Set(wave);
	pointPos->Set(pointPosition);
	waveMove->Set(isWaveMove);

}