#version 430
#extension GL_ARB_bindless_texture : enable

layout(std140) uniform CPU
{	
	vec4 CPU_color;
	vec3 posLight;
	vec3 coefMat;
	vec3 cameraPos;
	sampler2D samplerTexture;
};

in vec3 vNormale;
in vec3 myPos;
in vec3 texCoords;

layout (location = 0) out vec4 Color;

void main()
{
	
	vec4 col = CPU_color;
	col = texture(samplerTexture, texCoords.xy);

	vec3 vLight = normalize(myPos - posLight);
	float tetaLight = dot(-vLight, vNormale);

	vec3 vOrigine = normalize(cameraPos - myPos);

	vec3 vReflect = normalize(reflect(vLight, vNormale));
	float tetaVue = dot(vReflect, vOrigine);

	vec3 ambiant = col.xyz;
	
	vec3 diffuse = col.xyz * max(tetaLight, 0.0);
	
	vec3 specular = vec3(pow(max(tetaVue, 0.0), 64));

	vec3 colorLight = vec3(coefMat.x*ambiant + coefMat.y*diffuse + coefMat.z*specular);

	Color = vec4(colorLight, 0.5);

}