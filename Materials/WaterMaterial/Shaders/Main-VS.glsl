#version 430

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 

layout(std140) uniform CPU
{
	mat4 MVP;
	vec3 positionWave;
	vec3 pointPosition;
	float time;
	bool isWaveMove;
	bool methodWave;
	float speed;
};

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };

layout (location = 0) in vec3 Position;
layout (location = 2) in vec3 Normale;
layout (location = 3) in vec3 texCoord;

out vec3 vNormale;
out vec3 myPos;
out vec3 texCoords;

//fonction hash et noise : https://www.shadertoy.com/view/Ms2SD1
float hash( vec2 p ) {
	float h = dot(p,vec2(127.1,311.7));	
    return fract(sin(h)*43758.5453123);
}
float noise( in vec2 p ) {
    vec2 i = floor( p );
    vec2 f = fract( p );	
	vec2 u = f*f*(3.0-2.0*f);
    return 0.5*mix( mix( hash( i + vec2(0.0,0.0) ), 
                     hash( i + vec2(1.0,0.0) ), u.x),
                mix( hash( i + vec2(0.0,1.0) ), 
                     hash( i + vec2(1.0,1.0) ), u.x), u.y);
}

//fonction permettant de calculer la hauteur d'un point sur les vagues
float createWave(float amplitude, float frequence, vec2 direction, vec3 pos){

	float wave = 0.0;

	if(methodWave) //si on veut des vagues sinuso�dales
		wave = amplitude * sin(frequence * (direction.x * pos.x + direction.y * pos.z) + time + speed);
	else //sinon on veut des vagues circulaires
		wave = amplitude * sin(frequence * abs(distance(pos, pointPosition)) - (time + speed));
	return wave;
}

void main()
{
	vec3 vMyPos = Position;
	vec3 myTex = texCoord;
	vNormale = normalize(Normale);
	
	vec2 direction = vec2(1.0, 1.0);
	float frequence = 2.0;
	float amplitude = 0.25;

	float epsilon = 0.001;

	if(isWaveMove){ //Si on veut soit des vagues sinuso�dales, soit des vagues circulaires

		vec3 xPrime = vec3(vMyPos.x+epsilon, vMyPos.y, vMyPos.z);
		vec3 xPrimeMin = vec3(vMyPos.x-epsilon, vMyPos.y, vMyPos.z);
		vec3 zPrime = vec3(vMyPos.x, vMyPos.y, vMyPos.z+epsilon); 
		vec3 zPrimeMin = vec3(vMyPos.x, vMyPos.y, vMyPos.z-epsilon);

		float h = createWave(amplitude, frequence, direction, vMyPos);
		
		xPrime.y += createWave(amplitude, frequence, direction, xPrime);
		xPrimeMin.y += createWave(amplitude, frequence, direction, xPrimeMin);
		
		zPrime.y += createWave(amplitude, frequence, direction, zPrime);
		zPrimeMin.y += createWave(amplitude, frequence, direction, zPrimeMin);

		vMyPos.y += h;

		vec3 xVec = normalize(xPrime - xPrimeMin);
		vec3 zVec = normalize(zPrime - zPrimeMin);

		vNormale = normalize(-cross(xVec, zVec));

	}
	else{

		vec3 xPrime = vec3(vMyPos.x+epsilon, vMyPos.y, vMyPos.z);
		vec3 xPrimeMin = vec3(vMyPos.x-epsilon, vMyPos.y, vMyPos.z);
		vec3 zPrime = vec3(vMyPos.x, vMyPos.y, vMyPos.z+epsilon); 
		vec3 zPrimeMin = vec3(vMyPos.x, vMyPos.y, vMyPos.z-epsilon);

		vMyPos.y += noise(vMyPos.xz+time);

		xPrime.y += noise(xPrime.xz+time);
		xPrimeMin.y += noise(xPrimeMin.xz+time);
		zPrime.y += noise(zPrime.xz+time);
		zPrimeMin.y += noise(zPrimeMin.xz+time);

		vec3 xVec = normalize(xPrime - xPrimeMin);
		vec3 zVec = normalize(zPrime - zPrimeMin);

		vNormale = normalize(-cross(xVec, zVec));
	}

	myPos = vMyPos;
	gl_Position = MVP * vec4(myPos,1.0);
	texCoords = myTex;

}