#ifndef _WATERMATERIAL_H
#define _WATERMATERIAL_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class WaterMaterial : public MaterialGL
{
public:
	WaterMaterial(std::string name, glm::vec4 & c = glm::vec4(0.5, 0.5, 0.5, 1.0), bool isWaveMove = true);
	~WaterMaterial();
	void setColor(glm::vec4 & c);

	virtual void render(Node *o);
	virtual void update(Node* o, const int elapsedTime);

	void setCameraPos(glm::vec3 &camPos);
	void setPosLight(glm::vec3 &posLight);
	void setPosWave(glm::vec3 &posWave);
	void setPointPosition(glm::vec3 &pointPos);
	void setIsWaveMove(bool &isWaveMove);
	
	void setMethodWave(bool method);
	void setWaveSpeed(float speed);

private:
	GPUvec4* color;
	GPUmat4* modelViewProj;

	GPUvec3* light;

	GPUvec3* coef;
	GPUvec3* cameraPos;

	GPUvec3* pointPos;
	GPUvec3* positionWave;
	GPUfloat* timeCPU;
	GPUbool* waveMove;

	GPUTexture2D *waterTexture;
	GPUsampler *samplerTexture;

	GPUbool* gpuMethodWave;
	GPUfloat* gpuWaveSpeed;

	glm::vec3 posLight;
	glm::vec3 coefMat;

	glm::vec3 wave;
	glm::vec3 pointPosition;

	float time;
	bool timeProgress;
	bool isWaveMove;
	bool methodWave;

};

#endif