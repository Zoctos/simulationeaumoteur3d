#ifndef _WATERFALLMATERIAL_H
#define _WATERFALLMATERIAL_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class WaterfallMaterial : public MaterialGL
{
public:
	WaterfallMaterial(std::string name);
	~WaterfallMaterial();

	virtual void render(Node *o);
	virtual void update(Node* o, const int elapsedTime);

	void setCameraPos(glm::vec3 &camPos);
	void setPosLight(glm::vec3 &posLight);

private:
	GPUmat4* modelViewProj;

	GPUvec3* light;

	GPUvec3* coef;
	GPUvec3* cameraPos;

	GPUfloat* timeCPU;

	GPUTexture2D *waterTexture;
	GPUsampler *samplerTexture;

	glm::vec3 posLight;
	glm::vec3 coefMat;

	float time;

};

#endif