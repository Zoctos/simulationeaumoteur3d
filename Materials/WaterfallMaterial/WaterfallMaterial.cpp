#include "WaterfallMaterial.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


WaterfallMaterial::WaterfallMaterial(std::string name) :
	MaterialGL(name, "WaterfallMaterial")
{

	posLight = glm::vec3(0.0, 0.0, 0.0);
	coefMat = glm::vec3(0.2, 0.6, 0.2);

	time = 0.0f;

	timeCPU = vp->uniforms()->getGPUfloat("time");

	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	
	coef = fp->uniforms()->getGPUvec3("coefMat");
	coef->Set(coefMat);
	cameraPos = fp->uniforms()->getGPUvec3("cameraPos");
	cameraPos->Set(glm::vec3(1.0, 1.0, 1.0));

	light = fp->uniforms()->getGPUvec3("posLight");
	light->Set(posLight);

	waterTexture = new GPUTexture2D("C:/Home/Output/SampleProject/Debug/Textures/water.png");
	samplerTexture = fp->uniforms()->getGPUsampler("samplerTexture");
	samplerTexture->Set(waterTexture->getHandle());

	timeCPU->Set(0);

}
WaterfallMaterial::~WaterfallMaterial()
{

}

void WaterfallMaterial::setCameraPos(glm::vec3 &camPos)
{
	cameraPos->Set(camPos);
}

void WaterfallMaterial::setPosLight(glm::vec3 &posLight)
{
	light->Set(posLight);
}

void WaterfallMaterial::render(Node *o)
{
	if (m_ProgramPipeline)
	{
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void WaterfallMaterial::update(Node* o, const int elapsedTime)
{
	modelViewProj->Set(o->frame()->getTransformMatrix());

	time += 0.005;
	
	if (time > 1)
		time = 0;

	timeCPU->Set(time);
	

}