#version 430

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 

layout(std140) uniform CPU
{
	mat4 MVP;
	float time;
};

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };

layout (location = 0) in vec3 Position;
layout (location = 2) in vec3 Normale;
layout (location = 3) in vec3 texCoord;

out vec3 vNormale;
out vec3 myPos;
out vec3 texCoords;

//permet d'arrondir le sommet de la cascade
float waterfallPos(vec3 pos){
	float l = -log(1.0-pos.x);
	return l;
}

void main()
{
	vec3 vMyPos = Position;
	vNormale = normalize(Normale);
	
	myPos = vMyPos;
	myPos.y = waterfallPos(myPos);
	
	float epsilon = 0.001;

	vec3 xPrime = vec3(myPos.x+epsilon, waterfallPos(myPos), myPos.z);
	vec3 xPrimeMin = vec3(myPos.x-epsilon, waterfallPos(myPos), myPos.z);
	vec3 zPrime = vec3(myPos.x, waterfallPos(myPos), myPos.z+epsilon); 
	vec3 zPrimeMin = vec3(myPos.x, waterfallPos(myPos), myPos.z-epsilon);

	vec3 xVec = normalize(xPrime - xPrimeMin);
	vec3 zVec = normalize(zPrime - zPrimeMin);

	vNormale = normalize(cross(xVec, zVec));

	gl_Position = MVP * vec4(myPos,1.0);
	texCoords = texCoord;

	texCoords.x = mod(texCoords.x - time, 1);

}