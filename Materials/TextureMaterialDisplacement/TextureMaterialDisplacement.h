#ifndef _TEXTUREMATERIALDISPLACEMENT_H
#define _TEXTUREMATERIALDISPLACEMENT_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class TextureMaterialDisplacement : public MaterialGL
{
public:
	TextureMaterialDisplacement(std::string name, std::string texName, std::string heightmapName, Node *object, glm::vec3 posLight);
	~TextureMaterialDisplacement();

	virtual void render(Node *o);
	virtual void update(Node* o, const int elapsedTime);

	void setCameraPos(glm::vec3 &camera);
	void setLightPos(glm::vec3 &posLight);
	
	void setCoeff(float coeff);

private:
	
	GPUmat4* modelViewProj;

	GPUTexture2D* texture;
	GPUsampler* sampler;

	GPUTexture2D *heightMapTexture;
	GPUsampler* heightMap;
	
	GPUvec3* coef;
	GPUvec3* cameraPos;
	GPUvec3* light;

	GPUfloat* gpuCoeff;
	float coeff;

};

#endif