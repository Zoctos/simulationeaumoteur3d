#include "TextureMaterialDisplacement.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


TextureMaterialDisplacement::TextureMaterialDisplacement(std::string name, std::string texName, std::string heightMapName, Node *object, glm::vec3 posLight) :
	MaterialGL(name, "TextureMaterialDisplacement")
{

	modelViewProj = vp->uniforms()->getGPUmat4("MVP");

	texture = new GPUTexture2D("C:/Home/Output/SampleProject/Debug/Textures/"+texName);
	sampler = fp->uniforms()->getGPUsampler("samplerTexture");
	sampler->Set(texture->getHandle());

	heightMapTexture = new GPUTexture2D("C:/Home/Output/SampleProject/Debug/Textures/" + heightMapName);
	heightMap = vp->uniforms()->getGPUsampler("heightMap");
	heightMap->Set(heightMapTexture->getHandle());

	coef = fp->uniforms()->getGPUvec3("coefMat");
	cameraPos = fp->uniforms()->getGPUvec3("cameraPos");
	light = fp->uniforms()->getGPUvec3("posLight");
	coef->Set(glm::vec3(0.2, 0.6, 0.2));
	cameraPos->Set(Scene::getInstance()->camera()->convertPtTo(glm::vec3(0.0), object->frame()));
	light->Set(posLight);

	coeff = 10.0;
	gpuCoeff = vp->uniforms()->getGPUfloat("coeffHauteur");
	gpuCoeff->Set(coeff);

}
TextureMaterialDisplacement::~TextureMaterialDisplacement()
{

}

void TextureMaterialDisplacement::setCameraPos(glm::vec3 &camera)
{
	cameraPos->Set(camera);
}

void TextureMaterialDisplacement::setLightPos(glm::vec3 &posLight)
{
	light->Set(posLight);
}

void TextureMaterialDisplacement::setCoeff(float coeff) {
	gpuCoeff->Set(coeff);
}

void TextureMaterialDisplacement::render(Node *o)
{
	if (m_ProgramPipeline)
	{
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void TextureMaterialDisplacement::update(Node* o, const int elapsedTime)
{
	modelViewProj->Set(o->frame()->getTransformMatrix());
}