#version 430

#extension GL_ARB_shading_language_include : enable
#extension GL_ARB_bindless_texture : enable
#include "/Materials/Common/Common"
#line 6 


layout(std140) uniform CPU
{
	mat4 MVP;
	sampler2D heightMap;
	float coeffHauteur;
};

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };

layout (location = 0) in vec3 Position;
layout (location = 2) in vec3 Normale;
layout (location = 3) in vec3 textCoord;
layout (location = 4) in vec3 Tangent;

out vec3 texCoord;
out vec3 myPos;
out vec3 vNormale;
out vec3 vTangent;

//retourne la valeur a enlev� au y
float createHeight(float coeff, vec2 movement){
	vec2 tCoord = textCoord.xy;
	tCoord += movement;
	vec4 height = texture(heightMap, tCoord);
	return coeff*((height.r+height.g+height.b)/3.0);
}

void main()
{
	myPos = Position;
	vNormale = Normale;
	vTangent = Tangent;

	//On cr�e 4 points fictifs autour du point courant
	vec3 xPrime = vec3(myPos.x+0.01, myPos.y-createHeight(coeffHauteur, vec2(0.01, 0.0)), myPos.z);
	vec3 xPrimeMin = vec3(myPos.x-0.01, myPos.y-createHeight(coeffHauteur, vec2(-0.01, 0.0)), myPos.z);
	vec3 zPrime = vec3(myPos.x, myPos.y-createHeight(coeffHauteur, vec2(0.0, 0.01)), myPos.z+0.01); 
	vec3 zPrimeMin = vec3(myPos.x, myPos.y-createHeight(coeffHauteur, vec2(0.0, -0.01)), myPos.z-0.01);
	
	//On effectue la modification de notre point
	myPos.y -= createHeight(coeffHauteur, vec2(0.0));

	//On cr�e les deux vecteurs qui formeront notre plan
	vec3 xVec = normalize(xPrime - xPrimeMin);
	vec3 zVec = normalize(zPrime - zPrimeMin);

	//Et on r�cup�re la nouvelle normale
	vNormale = normalize(-cross(xVec, zVec));

	gl_Position = MVP * vec4(myPos,1.0);

	texCoord = textCoord;

}
