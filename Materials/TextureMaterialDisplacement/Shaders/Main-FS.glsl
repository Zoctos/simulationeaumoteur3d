#version 430 core
#extension GL_ARB_bindless_texture : enable

layout(std140) uniform CPU
{	
	sampler2D samplerTexture;
	vec3 cameraPos;
	vec3 posLight;
	vec3 coefMat;
};


in vec3 myPos;
in vec3 vNormale;
in vec3 texCoord;
in vec3 vTangent;

layout (location = 0) out vec4 Color;

void main()
{
	
	//Récupération des textures
	vec4 colTexture = texture(samplerTexture, texCoord.xy);
	
	vec3 norm = vNormale;

	//Calcul de phong 
	vec3 vLight = normalize(myPos - posLight);
	vec3 lightLocal = normalize(vLight);
	float tetaLight = dot(-lightLocal, norm);

	vec3 vOrigine = normalize(cameraPos - myPos);
	vec3 origineLocal = vOrigine;

	vec3 vReflect = normalize(reflect(lightLocal, norm));

	vec3 col = colTexture.xyz;

	float tetaVue = dot(vReflect, vOrigine);
	vec3 ambiant = col.xyz;
	
	vec3 diffuse = col.xyz * max(tetaLight, 0.0);
	
	vec3 specular = vec3(pow(max(tetaVue, 0.0), 8));

	vec3 colorPhong = vec3(coefMat.x*ambiant + coefMat.y*diffuse + coefMat.z*specular);

	Color = vec4(colorPhong.xyz, 1.0);

}