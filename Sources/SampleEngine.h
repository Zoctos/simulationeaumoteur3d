/*
 *	(c) XLim, UMR-CNRS
 *	Authors: G.Gilet
 *
 */

#ifndef _SAMPLE_ENGINE_H
#define _SAMPLE_ENGINE_H

#include <map>
#include <string>
#include "Engine/OpenGL/EngineGL.h"

#include <AntTweakBar\AntTweakBar.h>

#include "Effects\Pluie\Pluie.h"
#include "Effects/WaterFilter/WaterFilter.h"

class SampleEngine : public EngineGL
{
	public:
		SampleEngine(int width, int height);
		~SampleEngine();

		virtual bool init();
		virtual void render();
		virtual void animate(const int elapsedTime);
		virtual void onWindowResize(int width, int height);

protected:

private:

	glm::vec3 posLight = glm::vec3(0.0, 50.0, 0.0);

	bool timeProgress;
	bool lightMove;
	bool waveMove = false;
	bool transparentActive = true;
	bool isRain = false;
	float time = 0.0f;
	
	bool isPointMethod = true;
	float waveSpeed = 0.05;

	glm::vec3 pointPosition = glm::vec3(0.0, 0.0, 0.0);
	glm::vec3 wavePosition = glm::vec3(0.0, 0.0, 0.0);

	Pluie *pluie;

	WaterFilter *waterFilter;
	glm::vec3 filterColor = glm::vec3(0.0, 0.125, 0.25);

	float coefHauteur = 10.0f;
	
	GPUFBO* postProcess;

	TwBar* twBar;

};
#endif
