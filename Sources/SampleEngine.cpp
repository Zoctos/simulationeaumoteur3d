﻿/*
EngineGL overloaded for custom rendering
*/
#include "SampleEngine.h"
#include "Engine/OpenGL/v4/GLProgram.h"
#include "Engine/OpenGL/SceneLoaderGL.h"
#include "Engine/Base/NodeCollectors/MeshNodeCollector.h"
#include "Engine/Base/NodeCollectors/FCCollector.h"

#include <AntTweakBar/AntTweakBar.h>

#include "Materials/WaterMaterial/WaterMaterial.h"
#include "Materials/WaterfallMaterial/WaterfallMaterial.h"
#include "Materials/TextureMaterialDisplacement/TextureMaterialDisplacement.h"

#include "GPUResources/GPUInfo.h"

#include <glm\gtc\constants.hpp>

SampleEngine::SampleEngine (int width, int height) :
EngineGL (width, height)
{
	lightMove = true;

	//Initialisation et association des variables à la TweakBar
	twBar = TwNewBar("TweakBar");

	//Vagues
	TwAddSeparator(twBar, "", "");
	TwAddVarRW(twBar, "pointX", TW_TYPE_FLOAT, &pointPosition.x, "label='Point X' step=0.01 ");
	TwAddVarRW(twBar, "pointY", TW_TYPE_FLOAT, &pointPosition.y, "label='Point Y' step=0.01 min=0 ");
	TwAddVarRW(twBar, "pointZ", TW_TYPE_FLOAT, &pointPosition.z, "label='Point Z' step=0.01 ");
	TwAddVarRW(twBar, "avancementWave", TW_TYPE_FLOAT, &waveSpeed, "label='Avancement Vague' step=0.01 min=0 ");
	TwAddVarRW(twBar, "pointMethod", TW_TYPE_BOOLCPP, &isPointMethod, "label='Vague Methode'");
	TwAddVarRW(twBar, "moveWave", TW_TYPE_BOOLCPP, &waveMove, "label='Wave Move'");

	//Filtre
	TwAddSeparator(twBar, "", "");
	TwAddVarRW(twBar, "filterColorRed", TW_TYPE_FLOAT, &filterColor.r, "label='Red Filter' step=0.01 min=0 max=1");
	TwAddVarRW(twBar, "filterColorGreen", TW_TYPE_FLOAT, &filterColor.g, "label='Green Filter' step=0.01 min=0 max=1");
	TwAddVarRW(twBar, "filterColorBlue", TW_TYPE_FLOAT, &filterColor.b, "label='Blue Filter' step=0.01 min=0 max=1");

	//Transparence de l'eau et effet de pluie
	TwAddSeparator(twBar, "", "");
	TwAddVarRW(twBar, "transparentActive", TW_TYPE_BOOLCPP, &transparentActive, "label='Transparent'");
	TwAddVarRW(twBar, "rainActive", TW_TYPE_BOOLCPP, &isRain, "label='Pluie'");

	//Coefficient multiplicateur de la heightMap
	TwAddSeparator(twBar, "", "");
	TwAddVarRW(twBar, "sandCoeff", TW_TYPE_FLOAT, &coefHauteur, "label='Coefficient'");

	//Position de la lumière
	TwAddSeparator(twBar, "", "");
	TwAddVarRW(twBar, "lightX", TW_TYPE_FLOAT, &posLight.x, "label='Light X'");
	TwAddVarRW(twBar, "lightY", TW_TYPE_FLOAT, &posLight.y, "label='Light Y'");
	TwAddVarRW(twBar, "lightZ", TW_TYPE_FLOAT, &posLight.z, "label='Light Z'");
}

SampleEngine::~SampleEngine ()
{
}


bool SampleEngine::init ()
{
	LOG (INFO) << "Initializing Scene";
	timeQuery->create ();

	LOG(INFO) << GPUInfo::Instance()->getOpenGLVersion() << std::endl;

	// Load shaders in "\common" into graphic cards
	GLProgram::prgMgr.addPath (ressourceMaterialPath + "Common", "");

	postProcess = new GPUFBO("PostProcess");
	postProcess->create(w_Width, w_Height, 1, true, GL_RGBA32F, GL_TEXTURE_2D, 1);

	// Create Lighting Model GL and collect all light nodes of the scene 
	lightingModel = new LightingModelGL("LightingModel", scene->getRoot());
	// Bind the light buffer for rendering
	lightingModel->bind();

	//Plan de l'eau
	Node* plane = scene->getNode("WaterPlane");
	plane->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "plane100r.obj"));
	plane->setMaterial(new WaterMaterial("WaterMaterial",glm::vec4(0.2,0.3,0.5,0.5)));
	plane->frame()->translate(glm::vec3(0.0, -6.0, 0.0));
	plane->frame()->scale(glm::vec3(5.0));

	//Plan de la cascade
	Node* waterfall = scene->getNode("Waterfall");
	waterfall->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "plane100r.obj"));
	waterfall->setMaterial(new WaterfallMaterial("WaterfallMaterial"));
	waterfall->frame()->rotate(glm::vec3(0.0, 0.0, 1.0), (glm::pi<float>())/ 2);
	waterfall->frame()->translate(glm::vec3(-2.5, -23.25, 28.0));
	waterfall->frame()->scale(glm::vec3(0.5));

	//Plan du terrain
	Node *sand = scene->getNode("Sand");
	sand->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "plane100r.obj"));
	sand->setMaterial(new TextureMaterialDisplacement("TextureMaterialDisplacement", "Sand.png", "land.png", sand, posLight));
	sand->frame()->translate(glm::vec3(0.0, 16.0, 0.0));
	sand->frame()->scale(glm::vec3(5.0));

	scene->getSceneNode()->adopt(sand);
	scene->getSceneNode()->adopt(plane);
	scene->getSceneNode()->adopt(waterfall);

	//Création des effets postProcess
	pluie = new Pluie("Pluie", w_Width, w_Height);
	waterFilter = new WaterFilter("WaterFilter");

	// OpenGL state variable initialisation
	glClearColor (0.4, 0.4, 0.4, 1.0);
	glEnable (GL_DEPTH_TEST);

	// Force window Resize
	this->onWindowResize (w_Width, w_Height);

	allNodes->collect (scene->getRoot ());

	renderedNodes->collect (scene->getRoot ());

	for (unsigned int i = 0; i < allNodes->nodes.size (); i++)
		allNodes->nodes[i]->animate (0);

	return(true);
}


void SampleEngine::render ()
{
	
	// Begin Time query
	timeQuery->begin ();
	
	//On active le buffer de rendu
	postProcess->enable();
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Si on veut que la transparence soit activée
	if (transparentActive) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	postProcess->bindColorTexture(0);

	////Rendering every collected node
	for (unsigned int i = 0; i < renderedNodes->nodes.size (); i++)
		renderedNodes->nodes[i]->render();

	//On récupère la position de la caméra en fonction du plan représentant l'eau
	glm::vec3 p = scene->camera()->convertPtTo(glm::vec3(0.0, 0.0, 0.0), scene->getNode("WaterPlane")->frame());

	//Si on est sous l'eau on applique le filtre
	if (p.y <= 0.25) {
		waterFilter->setFilterColor(filterColor);
		waterFilter->apply(postProcess);
	}
	else if(isRain){ //Sinon si la pluie est activé on affiche la pluie
		pluie->apply(postProcess);
	}

	postProcess->getColorTexture(0)->release();

	if (drawLights)
		lightingModel->renderLights();

	if (drawBoundingBoxes)
		for (unsigned int i = 0; i < renderedNodes->nodes.size(); i++)
			renderedNodes->nodes[i]->render(boundingBoxMat);

	//On désactive le buffer et on affiche ce qu'il contient
	postProcess->disable();
	postProcess->display();

	// end time Query					
	timeQuery->end ();

	scene->needupdate = false;

	TwDraw();

}
void SampleEngine::animate (const int elapsedTime)
{
	
	// Animate each node
	for (unsigned int i = 0; i < allNodes->nodes.size (); i++)
		allNodes->nodes[i]->animate (elapsedTime);

	if (timeProgress) {
		time++;
		wavePosition.x += 0.001;
	}
	else {
		time--;
		wavePosition.x -= 0.001;
	}

	if (time > 1000 || time < -2000)
		timeProgress = !timeProgress;

	//Mise à jour des variables des matériaux de la scène
	glm::vec3 p = scene->camera()->convertPtTo(glm::vec3(0.0, 0.0, 0.0), scene->getNode("WaterPlane")->frame());
	glm::vec3 pLight = scene->getRoot()->frame()->convertPtTo(posLight, scene->getNode("WaterPlane")->frame());
	(dynamic_cast<WaterMaterial*>(scene->getNode("WaterPlane")->getMaterial()))->setPosLight(pLight);
	(dynamic_cast<WaterMaterial*>(scene->getNode("WaterPlane")->getMaterial()))->setCameraPos(p);
	(dynamic_cast<WaterMaterial*>(scene->getNode("WaterPlane")->getMaterial()))->setPosWave(wavePosition);
	(dynamic_cast<WaterMaterial*>(scene->getNode("WaterPlane")->getMaterial()))->setPointPosition(pointPosition);
	(dynamic_cast<WaterMaterial*>(scene->getNode("WaterPlane")->getMaterial()))->setIsWaveMove(waveMove);

	(dynamic_cast<WaterMaterial*>(scene->getNode("WaterPlane")->getMaterial()))->setMethodWave(isPointMethod);
	(dynamic_cast<WaterMaterial*>(scene->getNode("WaterPlane")->getMaterial()))->setWaveSpeed(waveSpeed);

	p = scene->camera()->convertPtTo(glm::vec3(0.0, 0.0, 0.0), scene->getNode("Waterfall")->frame());
	pLight = scene->getRoot()->frame()->convertPtTo(posLight, scene->getNode("Waterfall")->frame());
	(dynamic_cast<WaterfallMaterial*>(scene->getNode("Waterfall")->getMaterial()))->setPosLight(pLight);
	(dynamic_cast<WaterfallMaterial*>(scene->getNode("Waterfall")->getMaterial()))->setCameraPos(p);

	p = scene->camera()->convertPtTo(glm::vec3(0.0, 0.0, 0.0), scene->getNode("Sand")->frame());
	pLight = scene->getRoot()->frame()->convertPtTo(posLight, scene->getNode("Sand")->frame());
	(dynamic_cast<TextureMaterialDisplacement*>(scene->getNode("Sand")->getMaterial()))->setLightPos(pLight);
	(dynamic_cast<TextureMaterialDisplacement*>(scene->getNode("Sand")->getMaterial()))->setCameraPos(p);
	(dynamic_cast<TextureMaterialDisplacement*>(scene->getNode("Sand")->getMaterial()))->setCoeff(coefHauteur);

}

void SampleEngine::onWindowResize (int width, int height)
{

	glViewport (0, 0, width, height);
	scene->camera ()->setAspectRatio ((float)width / (float)height);
	w_Width = width;
	w_Height = height;


}
